from django.db import models

# Create your models here.
class Patient(models.Model):
    name = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=50)
    address = models.TextField()
