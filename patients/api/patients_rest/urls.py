from django.urls import path

from .views import (
    api_patients,
)

urlpatterns = [
    path(
        "patients/",
        api_patients,
        name="api_doctors",
    ),
]