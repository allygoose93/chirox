from telnetlib import DO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Patient

# Patient encoder
class PatientEncoder(ModelEncoder):
    model = Patient
    properties = [
        "name",
        "phone_number",
        "address",
    ]

# get and post method
@require_http_methods(["GET", "POST"])
def api_patients(request):
    if request.method == "GET":
        patients = Patient.objects.all()
        return JsonResponse(
            {"patients": patients},
            encoder=PatientEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            patient = Patient.objects.create(**content)
            return JsonResponse(
                patient,
                encoder=PatientEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"message": "Missing patient"})
            response.status_code = 400
            return response

@require_http_methods(["GET", "PUT", "DELETE"])
def api_patient(request, pk):
    if request.method == "GET":
        try:
            patient = Patient.objects.get(id=pk)
            return JsonResponse(
                patient,
                encoder=PatientEncoder,
                safe=False,
            )
        except Patient.DoesNotExist:
            response = JsonResponse({"message": "No patient existe profavor"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            patient = Patient.objects.get(id=pk)
            patient.delete()
            return JsonResponse(
                patient,
                encoder=PatientEncoder,
                safe=False,
            )
        except Patient.DoesNotExist:
            response = JsonResponse({"message": "No patient existe profavor"})
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body)
            patient = Patient.objects.get(id=pk)

            props = [
                "name",
                "phone_number",
                "address",
            ]
            for prop in props:
                if prop in content:
                    setattr(patient, prop, content[prop])
            patient.save()
            return JsonResponse(
                patient,
                encoder=PatientEncoder,
                safe=False,
            )
        except Patient.DoesNotExist:
            response = JsonResponse({"message": "No patient existe profavor"})
            response.status_code = 404
            return response