from telnetlib import DO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Doctor

# doctor encoder
class DoctorEncoder(ModelEncoder):
    model = Doctor
    properties = [
        "name",
        "treatment",
    ]

# get and post method
@require_http_methods(["GET", "POST"])
def api_doctors(request):
    if request.method == "GET":
        doctors = Doctor.objects.all()
        return JsonResponse(
            {"doctors": doctors},
            encoder=DoctorEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            doctor = Doctor.objects.create(**content)
            return JsonResponse(
                doctor,
                encoder=DoctorEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"message": "Did you eat an apple, because the doctor is not here"})
            response.status_code = 400
            return response

@require_http_methods(["GET", "PUT", "DELETE"])
def api_doctor(request, pk):
    if request.method == "GET":
        try:
            doctor = Doctor.objects.get(id=pk)
            return JsonResponse(
                doctor,
                encoder=DoctorEncoder,
                safe=False,
            )
        except Doctor.DoesNotExist:
            response = JsonResponse({"message": "No existe profavor"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            doctor = Doctor.objects.get(id=pk)
            doctor.delete()
            return JsonResponse(
                doctor,
                encoder=DoctorEncoder,
                safe=False,
            )
        except Doctor.DoesNotExist:
            response = JsonResponse({"message": "No existe profavor"})
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body)
            doctor = Doctor.objects.get(id=pk)

            props = [
                "name",
                "treatment",
            ]
            for prop in props:
                if prop in content:
                    setattr(doctor, prop, content[prop])
            doctor.save()
            return JsonResponse(
                doctor,
                encoder=DoctorEncoder,
                safe=False,
            )
        except Doctor.DoesNotExist:
            response = JsonResponse({"message": "No existe profavor"})
            response.status_code = 404
            return response
