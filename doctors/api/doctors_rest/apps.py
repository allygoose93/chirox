from django.apps import AppConfig


class DoctorsRestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'doctors_rest'
