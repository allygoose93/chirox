from django.urls import path

from .views import (
    api_doctors,
)

urlpatterns = [
    path(
        "doctors/",
        api_doctors,
        name="api_doctors",
    ),
]