## personal notes
To create the project
> mkdir -p doctors/api
> django-admin startproject doctors_project doctors/api/

Create apps
> cd doctors/api/
> python manage.py startapp doctors_rest
>create file doctors/api/requirements.txt

asgiref==3.5.0
black==22.3.0
certifi==2021.10.8
charset-normalizer==2.0.12
click==8.1.2
dj-database-url==0.5.0
django-cors-headers==3.11.0
Django==4.0.3
djwto==0.0.3
flake8==4.0.1
idna==3.3
mccabe==0.6.1
mypy-extensions==0.4.3
pathspec==0.9.0
platformdirs==2.5.1
psycopg2==2.9.3
pycodestyle==2.8.0
pyflakes==2.4.0
requests==2.27.1
sqlparse==0.4.2
tomli==2.0.1
urllib3==1.26.9
whitenoise==5.3.0

>create doctors/api/Dockerfile.dev 
FROM python:3.10-bullseye
ENV PYTHONUNBUFFERED 1
WORKDIR /app
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.9.0/wait /wait
RUN chmod +x /wait
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
CMD /wait && python manage.py migrate && python manage.py runserver "0.0.0.0:8000"

> repeat for other microservices
> after writing your models register it in admin

Create Database user for makemigrations

> Run docker exec -it «api-container-name» bash to connect to the running service that contains an API service
> Run python manage.py createsuperuser at the container command prompt.
> Run python manage.py makemigrations at the container command prompt. Make sure it generates the migration you expected.
> Run python manage.py migrate to apply the migration.